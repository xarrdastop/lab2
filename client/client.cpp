#include <stdio.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <unistd.h>
#include <cstring>
#include <fstream>

using namespace std;

#pragma pack(push, 1)
struct TLV {
	uint64_t length;
	char type;
	char value[0];
};
#pragma pack(pop)

bool sendSome(int to, char* data, size_t size)
{
	size_t current_send_bytes = 0;
	
	while(size != 0)
	{
		current_send_bytes = send(to, data, sizeof(size), 0);
		
		if (current_send_bytes == -1)
		{
			printf("current_send_bytes error: %d\n", errno);
			
			return false;
		}
		
		data += current_send_bytes;
		size -= current_send_bytes;
	}
	
	return true;
}

/*
bool receiveSome(int from, char* data, size_t size)
{
	size_t current_recv_bytes = 0;
	
	while(size != 0)
	{
		current_recv_bytes = recv(from, data, sizeof(size), 0);
		
		if (current_recv_bytes == -1)
		{
			printf("current_recv_bytes error: %d\n", errno);
			
			return false;
		}
		
		data += current_recv_bytes;
		size -= current_recv_bytes;
		
		printf("current_recv_bytes: %d\n", current_recv_bytes);
	}
	
	return true;
}
*/

bool receiveSome(int from, char* data)
{
	cout << "START RECIEVE" << endl;
	ssize_t current_recv_bytes = 0;
	uint64_t needed_bytes_count = 0;
	
	struct TLV* tlv_request_file_ptr = (struct TLV*) data;
	
	current_recv_bytes = recv(from, &tlv_request_file_ptr->length, sizeof(TLV::length), 0);
	std::cout << "needed bytes count=" << tlv_request_file_ptr->length << std::endl;
	needed_bytes_count = tlv_request_file_ptr->length;
	
	current_recv_bytes = recv(from, &tlv_request_file_ptr->type, sizeof(TLV::type), 0);
	std::cout << "msg type=" << tlv_request_file_ptr->type << std::endl;
	needed_bytes_count -= current_recv_bytes; 
	
	std::cout << "needed bytes count=" << needed_bytes_count << std::endl;
	
	while(needed_bytes_count != 0)
	{
		cout << "position: " << tlv_request_file_ptr->length - needed_bytes_count << endl;
		ssize_t current_recv_bytes = recv(from, &tlv_request_file_ptr->value[tlv_request_file_ptr->length - needed_bytes_count - 1], needed_bytes_count, 0);
		
		if (current_recv_bytes == -1)
		{
			printf("current_recv_bytes error: %d\n", errno);
			
			return false;
		}
		
		needed_bytes_count -= current_recv_bytes;

		cout << "current_recv_bytes: " << current_recv_bytes << std::endl;
	}
	std::cout << "data: " << tlv_request_file_ptr->value << std::endl;

cout << "save file" << endl;
        ofstream output("file", ofstream::out);
        output.write(tlv_request_file_ptr->value, tlv_request_file_ptr->length-1);
        output.close();
 
	cout << "END RECIEVE" << endl;
	return true;
}



int main(int argc, char **argv)
{
	int sfd;
	
	// Инициализируем сокет
	sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	int setsockopt_opt = 1;
	setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&setsockopt_opt, sizeof(setsockopt_opt));
	
	// Проверка на ошибку при открытии
	if (sfd == -1)
	{
		printf("socket errno: %d\n", errno);
	}
	
	// Строковое значение ip адреса для ввода из консоли
	string s_ip = "";
	// Порт соединения
	unsigned short int port = 0;
	
	// Ввод ip адреса
	printf("Enter ip address: ");
	cin >> s_ip;
	cout << "IP address: " << s_ip << endl;
	const char * c_ip = s_ip.c_str();
	
	// Ввод порта
	printf("Enter port: ");
	cin >> port;
	cout << "Port: " << port << endl;
	
	// Структура для bind'a сокета
	struct sockaddr_in addr;

	// Указываем семейство ip_v4
	addr.sin_family = AF_INET;
	
	// Указываем порт
	addr.sin_port = htons(port);
	
	// Указываем ip адрес
	inet_pton(AF_INET, c_ip, &(addr.sin_addr));
	
	// Устанавливаем соединение клиента с сервером
	if (connect(sfd, (const sockaddr *)&addr, sizeof(addr)) == -1)
	{
		printf("connect errno: %d\n", errno);
		return -1;
	}
	
	string console_input;
	char buf[5000];
	//struct TLV tlv_request_file;
	struct TLV* tlv_request_file_ptr = (struct TLV*) buf;
	
	char file_buf[5000];
	struct TLV* tlv_file_ptr = (struct TLV*) file_buf;
	while(true)
	{
		cout << "Enter name of remote file or /quit to exit.\n";
		cin >> console_input;
		
		tlv_request_file_ptr->type = 'R';
		memcpy(tlv_request_file_ptr->value, console_input.c_str(), console_input.length());
		tlv_request_file_ptr->length = console_input.length() + sizeof(TLV::type);
		
		sendSome(sfd, (char*)tlv_request_file_ptr, sizeof(buf));
		
		//memset(&tlv_request_file_ptr, 0, sizeof(tlv_request_file_ptr));
		
		receiveSome(sfd, (char*)tlv_file_ptr);
	}
	
	return 0;
}