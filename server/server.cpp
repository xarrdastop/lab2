#include <stdio.h>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>
#include <cstring>
#include <cstdint>
#include <fstream>
#include <stdexcept>
#include <vector>

using namespace std;

#pragma pack(push, 1)
struct TLV {
	uint64_t length;
	char type;
	char value[0];
};
#pragma pack(pop)

std::vector<char> readFile(const char* path)
{
    using namespace std;
    // Флаг binary критически важен как при считывании, так и при записи
    // двоичных файлов (изображений и т. п.)!
    ifstream input(path, ios::in | ios::binary);
    if (!input) {
        throw runtime_error("File inaccessible!");
    }
    input.seekg(0, ios::end);
    auto const size = input.tellg();
    vector<char> content(size);
    input.seekg(0, ios::beg);
    input.read(content.data(), size);
    return content;
}

bool receiveSome(int from, char* data)
{
	cout << "START RECIEVE" << endl;
	ssize_t current_recv_bytes = 0;
	uint64_t needed_bytes_count = 0;
	
	struct TLV* tlv_request_file_ptr = (struct TLV*) data;
	
	current_recv_bytes = recv(from, &tlv_request_file_ptr->length, sizeof(TLV::length), 0);
	std::cout << "needed bytes count=" << tlv_request_file_ptr->length << std::endl;
	needed_bytes_count = tlv_request_file_ptr->length;
	
	current_recv_bytes = recv(from, &tlv_request_file_ptr->type, sizeof(TLV::type), 0);
	std::cout << "msg type=" << tlv_request_file_ptr->type << std::endl;
	needed_bytes_count -= current_recv_bytes; 
	
	std::cout << "needed bytes count=" << needed_bytes_count << std::endl;
	
	while(needed_bytes_count != 0)
	{
		cout << "position: " << tlv_request_file_ptr->length - needed_bytes_count << endl;
		ssize_t current_recv_bytes = recv(from, &tlv_request_file_ptr->value[tlv_request_file_ptr->length - needed_bytes_count - 1], needed_bytes_count, 0);
		
		if (current_recv_bytes == -1)
		{
			printf("current_recv_bytes error: %d\n", errno);
			
			return false;
		}
		
		needed_bytes_count -= current_recv_bytes;

		//cout << "current_recv_bytes: " << current_recv_bytes << std::endl;
	}
	std::cout << "data: " << tlv_request_file_ptr->value << std::endl; 
	cout << "END RECIEVE" << endl;
	return true;
}

bool sendSome(int to, char* data, size_t size)
{
	size_t current_send_bytes = 0;
	
	while(size != 0)
	{
		current_send_bytes = send(to, data, sizeof(size), 0);
		
		if (current_send_bytes == -1)
		{
			printf("current_send_bytes error: %d\n", errno);
			
			return false;
		}
		
		data += current_send_bytes;
		size -= current_send_bytes;
	}
	
	return true;
}

int main(int argc, char **argv)
{
	int sfd;
	
	// Инициализируем сокет
	sfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	
	// Устанавливаем опцию SO_REUSEADDR для дуплексного использования ip + port
	int setsockopt_opt = 1;
	setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (const char*)&setsockopt_opt, sizeof(setsockopt_opt));
	
	// Проверка на ошибку при открытии
	if (sfd == -1)
	{
		printf("socket errno: %d\n", errno);
	}
	
	// Строковое значение ip адреса для ввода из консоли
	string s_ip = "";
	// Порт соединения
	unsigned short int port = 0;
	
	// Ввод ip адреса
	printf("Enter ip address: ");
	cin >> s_ip;
	cout << "IP address: " << s_ip << endl;
	const char * c_ip = s_ip.c_str();
	
	// Ввод порта
	printf("Enter port: ");
	cin >> port;
	cout << "Port: " << port << endl;
	
	// Структура для bind'a сокета
	struct sockaddr_in addr;
	
	// Указываем семейство ip_v4
	addr.sin_family = AF_INET;
	
	// Указываем порт
	addr.sin_port = htons(port);
	
	// Указываем ip адрес
	inet_pton(AF_INET, c_ip, &(addr.sin_addr));
	
	// Связываем сокет с ip адресом и портом
	if (bind(sfd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
	{
		printf("bind errno: %d\n", errno);
		return 1;
	}
	
	// Включение режима слушателя"
	if (listen(sfd, SOMAXCONN) == -1)
	{
		printf("listen errno: %d\n", errno);
		return 1;
	}
	else
	{
		printf("listen\n");
	}
	
	char buf[5000];
	struct TLV* tlv_request_file_ptr = (struct TLV*) buf;
	
	// Ожидание клиента
	while(true)
	{
		printf("listen continue\n");
		// Адрес подключенного клиента
		struct sockaddr client_addr;
		socklen_t client_addr_len;
		
		// Подключение сервером клиента
		int client_sock = accept(sfd, &client_addr, &client_addr_len);
		if (client_sock == -1)
		{
			printf("accept errno: %d\n", errno);
		}
		else
		{
			printf("accept\n");
		}
		
		receiveSome(client_sock, (char*)tlv_request_file_ptr);
		
		cout << "Get file name:" << endl;
		cout << "tlv_request_file_ptr->length: " << tlv_request_file_ptr->length << endl;
		cout << "tlv_request_file_ptr->type: " << tlv_request_file_ptr->type << endl;
		cout << "tlv_request_file_ptr->value: " << tlv_request_file_ptr->value << endl;
		
		// if (type == 'R') ...
		vector<char> file_content = readFile(tlv_request_file_ptr->value);
		uint64_t file_size = file_content.size();
		
        cout << "file_size:" << file_size << endl; 
        cout << "begin file" << endl;
        for (std::vector<char>::const_iterator i = file_content.begin(); i != file_content.end(); ++i)
            std::cout << *i << ' ';
        cout << "end file" << endl;
		
		tlv_request_file_ptr->type = 'F';
		memcpy(tlv_request_file_ptr->value, &file_content[0], file_size);
		tlv_request_file_ptr->length = file_size + sizeof(TLV::type);
		
		cout << "Send file:" << endl;
		cout << "tlv_request_file_ptr->length: " << tlv_request_file_ptr->length << endl;
		cout << "tlv_request_file_ptr->type: " << tlv_request_file_ptr->type << endl;
		cout << "tlv_request_file_ptr->value: " << tlv_request_file_ptr->value << endl;
		
		cout << "tlv_request_file_ptr size: " << sizeof(tlv_request_file_ptr) << endl;
		sendSome(client_sock, (char*)tlv_request_file_ptr, sizeof(buf));
        
        
		
	}
	
	return 0;
}